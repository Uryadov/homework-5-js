function createNewUser() {

    const Name = prompt('Enter your name', 'name');
    const surName = prompt('Enter your surname', 'surname');
    const Birthday = prompt('Enter your birthday', 'dd.mm.yyyy');

    const newUser = {
        firstName: Name,
        lastName: surName,
        birthday: Birthday,
        getPassword: function () {
        return (this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + (this.birthday.substr(this.birthday.length - 4)))
        },
        getAge: function () {
            const date = new Date().getFullYear();
            return date - (this.birthday.substr(this.birthday.length - 4))
        }
    };

    return newUser;

}

const user = createNewUser();
console.log(user.getPassword());
console.log(user.getAge());
console.log(`User is ${user.getAge()} years old`);
